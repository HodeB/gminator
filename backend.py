from PyQt5.QtWidgets import QListWidgetItem
from PyQt5.QtCore import Qt

import copy
import pprint

from load import Load
from os import getcwd


class Session():
    def __init__(self, master):
        self.master = master

        self.effects = {}

        self.keyFrames = {0: {}}
        self.selectedFrame = 0
        self.selectedFrameContainsKeyframe = True
        self.lastSaveFileName = 'script.py'
        self.outputPath = getcwd() + '/outputDir/'
        self.size = (640, 360)
        self.fps = 12

    def initLayers(self):
        self.selectedLayer = 'Root'
        self.layers = {'Root': {'item': QListWidgetItem('Root'),
                                'blendMode': 'Normal',
                                'opacity': 100,
                                'inputMode': None,
                                'inputPath': None,
                                'lastScrollPosition': 0}}
        self.layers['Root']['item'].setFlags(Qt.ItemIsEnabled |
                                             Qt.ItemIsSelectable)

        self.master.layersList.addItem(self.layers['Root']['item'])
        self.master.selectLayer('Root')

    def addLayer(self, name, item):
        if name not in self.layers.keys():
            self.layers[name] = {'item': item,
                                 'blendMode': 'Normal',
                                 'opacity': 100,
                                 'inputMode': None,
                                 'inputPath': None,
                                 'lastScrollPosition': 0}
        else:
            self.layers[name]['item'] = item
            self.layers[name]['lastScrollPosition'] = 0

    def delLayer(self, name):
        for item in self.getEffects(self.selectedLayer):
            item['widget'].deleteLater()
            self.effects = {key: effect for key, effect in self.effects.items() if effect is not item}

        self.layers.pop(name)

    def addEffect(self):
        effectId = len(self.effects.keys())
        self.effects[effectId] = {'layer': self.selectedLayer}

        return effectId

    def updateEffect(self, effectId, name, value):
        self.addKeyFrame(self.selectedFrame, effectId, name, value)

    def getEffects(self, layer):
        resDict = {}
        for effectId, effect in self.effects.items():
            if effect['layer'] == layer:
                resDict.update({effectId: effect})
        return resDict

    def renameEffect(self, effectId, effect, attr):
        self.effects[effectId]['effect'] = effect

        for frame, item in self.keyFrames.items():
            if effectId in item.keys():
                if frame == 0:
                    item[effectId] = copy.deepcopy(attr)
                else:
                    item.pop(effectId)

        self.updateKeyFrames()

    def updateKeyFrames(self):
        toPop = []

        for frame, item in self.keyFrames.items():
            if len(item.keys()) < 2:
                item['marker'].deleteLater()
                toPop.append(frame)

        for frame in toPop:
            self.keyFrames.pop(frame)

    def selectLayer(self, layer):
        if layer in self.layers.keys():
            self.selectedLayer = layer
            self.layers[layer]['lastScrollPosition'] = self.master.effectScrollArea.horizontalScrollBar().value()
            return True

        return False

    def selectFrame(self, frame):
        self.selectedFrame = frame
        self.populateEditors(self.getFrame(frame))
        self.master.timeLine.selectFrame(frame)

        if frame in self.keyFrames.keys():
            self.populateMarkers(self.keyFrames[frame])
        else:
            self.populateMarkers({})

    def populateEditors(self, effects):
        for effectId, attr in effects.items():
            self.effects[effectId]['editor'].clearEditor()
            self.effects[effectId]['editor'].setupEditor(attr)

    def populateMarkers(self, keyframe):
        ignore = []
        for effectId, attr in keyframe.items():
            if effectId != 'marker':
                self.effects[effectId]['editor'].setupMarkers(attr)
                ignore.append(effectId)

        for effectId, effect in self.effects.items():
            if effectId not in ignore:
                effect['editor'].setupMarkers({})

    def getLastValueAtFrame(self, targetFrame, effectId, name):
        frame = 0
        lastKey = -1

        for key, keyFrame in self.keyFrames.items():
            if key != 0 and key < targetFrame and key > lastKey:
                if effectId in keyFrame.keys():
                    if name in keyFrame[effectId].keys():
                        frame = key
                        lastKey = key

        value = self.keyFrames[frame][effectId][name]

        return frame, value

    def getNextValueAtFrame(self, targetFrame, effectId, name):
        frame = 0
        lastKey = max(self.keyFrames.keys()) + 1

        for key, keyFrame in self.keyFrames.items():
            if key != 0 and key > targetFrame and key < lastKey:
                if effectId in keyFrame.keys():
                    if name in keyFrame[effectId].keys():
                        frame = key
                        lastKey = key

        value = self.keyFrames[frame][effectId][name]

        return frame, value

    def getValueAtPoint(self, frame, effectId, name):
        lastFrame, lastValue = self.getLastValueAtFrame(frame, effectId, name)
        nextFrame, nextValue = self.getNextValueAtFrame(frame, effectId, name)

        value = lastValue
        if frame > nextFrame:
            pass
        elif lastFrame != nextFrame:
            frameDistance = frame - lastFrame
            valPerFrame = 0

            if lastValue < nextValue:
                valPerFrame = (nextValue - lastValue) / (nextFrame - lastFrame)
            elif lastValue > nextValue:
                valPerFrame = (lastValue - nextValue) / (nextFrame - lastFrame)
                valPerFrame *= -1

            value = lastValue + (valPerFrame * frameDistance)

        return value

    def getFrame(self, frame):
        resDict = {}
        for effectId, effectAttr in self.keyFrames[0].items():
            if effectId != 'marker':
                resDict[effectId] = {}
                for name, value in effectAttr.items():
                    try:
                        resDict[effectId][name] = self.keyFrames[frame][effectId][name]
                    except KeyError:
                        resDict[effectId][name] = self.getValueAtPoint(frame, effectId, name)

        return resDict

    def addKeyFrame(self, frame, effectId, name, value):
        if frame == 0 and effectId in self.keyFrames[0].keys():
            self.keyFrames[0][effectId][name] = value

        elif frame not in self.keyFrames.keys():
            self.keyFrames[frame] = {effectId: {name: value}}

            if 'marker' not in self.keyFrames[frame].keys():
                self.keyFrames[frame]['marker'] = self.master.timeLine.addMarker()

        else:
            if effectId in self.keyFrames[frame].keys():
                self.keyFrames[frame][effectId].update({name: value})
            else:
                self.keyFrames[frame][effectId] = {name: value}

        if frame == self.selectedFrame:
            self.selectedFrameContainsKeyframe = True

    def write(self, fileName):
        result = "from userfacing import (add, play, keyframe, animate, setOutputDir,\n\
                        openImage, setSize, setFps, addLayer,\n\
                        useLayer, animateReturn, getSize, getFps,\n\
                        delLayer, openVideo, openImageStack, openLayer)\n\n\
setSize({size})\n\
setOutputDir('{outputPath}')\n\
setFps({fps})\n\n".format(size=str(self.size)[1:-1], outputPath=self.outputPath, fps=self.fps)

        for layerName, layer in self.layers.items():
            if layerName != 'Root':
                result += "addLayer('{}', '{}', {})\n".format(layerName, layer['blendMode'].lower(), layer['opacity'] / 100)
                result += "useLayer('{}')\n".format(layerName)

            if layer['inputMode'] == 'Clone':
                mode = 'Layer'
            elif layer['inputMode'] == 'Image':
                mode = 'Image'
            elif layer['inputMode'] == 'Video':
                mode = 'Video'

            result += "open{}('{}')\n".format(mode, layer['inputPath'])

            for effectId, effect in self.getEffects(layerName).items():
                result += "add('{}', '{}', ".format(effectId, self.effects[effectId]['effect'])

                indent = 4
                attr = '{}'.format({})[0]
                attr += '\n'
                for name, value in self.keyFrames[0][effectId].items():
                    attr += ' ' * indent
                    attr += "'{}': {},\n".format(name, value)
                attr = attr[:-2]
                attr += '{}'.format({})[1] + ')'

                result += '{}\n'.format(attr)
            result += '\n'

        keyFrames = {}
        for frame in range(self.master.timeLine.frames):
            if frame in self.keyFrames.keys() and frame > 0:
                effects = self.keyFrames[frame]

                for layerName, layer in self.layers.items():
                    for effectId, effect in self.getEffects(layerName).items():
                        if effectId in effects.keys() and effectId != 'marker':

                            for name, value in effects[effectId].items():
                                f = self.getLastValueAtFrame(frame, effectId, name)[0]
                                if f not in keyFrames.keys():
                                    keyFrames[f] = {effectId: {name: value}}
                                elif effectId not in keyFrames[f].keys():
                                    keyFrames[f][effectId] = {name: value}
                                else:
                                    keyFrames[f][effectId][name] = value

        for frame in range(self.master.timeLine.frames):
            if frame in keyFrames.keys():
                effects = keyFrames[frame]

                if frame != 0:
                    result += '\nkeyframe({})\n'.format(frame)

                for layerName, layer in self.layers.items():
                    newLayer = True
                    for effectId, effect in self.getEffects(layerName).items():
                        if effectId in effects.keys() and effectId != 'marker':
                            if newLayer:
                                result += "useLayer('{}')\n".format(layerName)
                                newLayer = False

                            indent = 4
                            for name, value in effects[effectId].items():
                                attr = '{}'.format({})[0]
                                result += "animate('{}', {}, ".format(effectId, self.getNextValueAtFrame(frame, effectId, name)[0] - frame)
                                attr += "'{}': {}".format(name, value)
                                attr += '{}'.format({})[1] + ')'

                                result += '{}\n'.format(attr)

        result += '\nkeyframe({})\n'.format(max(self.keyFrames.keys()))
        result += 'play()'

        with open(fileName, 'w') as f:
            f.write(result)

    def save(self, fileName=None):
        if not fileName:
            fileName = self.lastSaveFileName
        self.lastSaveFileName = fileName

        self.write(fileName)

        self.master.status.setText('Saved!')
        self.master.delay(self.master.status.clear, 700)

    def load(self, path):
        self.selectFrame(0)
        self.lastSaveFileName = path
        self.master.clear()

        newState = Load(path)

        self.effects = newState.effects
        self.layers = newState.layers
        self.keyFrames = newState.keyframes
        self.outputPath = newState.outputPath
        self.size = newState.size
        self.master.FPSSpinBox.setValue(newState.fps)

        self.selectedLayer = 'Root'
        self.layers['Root'] = {'item': QListWidgetItem('Root'),
                               'blendMode': 'Normal',
                               'opacity': 100,
                               'inputMode': newState.layers['Root']['inputMode'],
                               'inputPath': newState.layers['Root']['inputPath'],
                               'lastScrollPosition': 0}
        self.layers['Root']['item'].setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable)
        self.master.layersList.addItem(self.layers['Root']['item'])

        self.master.drawKeyframes()
        self.master.drawLayers()
        self.master.drawEffects()

        self.master.selectLayer('Root')
