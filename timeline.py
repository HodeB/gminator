from PyQt5.QtWidgets import (QHBoxLayout, QLabel, QSpinBox, QGraphicsView,
                             QGraphicsScene, QAbstractSpinBox)
from PyQt5.QtSvg import QSvgWidget
from PyQt5.QtCore import Qt
from math import floor


class TimeLineScene(QGraphicsScene):
    def __init__(self, timeline, height, session, *args, **kwargs):
        super(TimeLineScene, self).__init__(*args, **kwargs)

        self.timeline = timeline
        self.height = height
        self.session = session

        self.childLines = []
        self.childLabels = []

    def drawWidgets(self):
        self.timeLineBackground = QSvgWidget('graphics/timeLineBackground.svg')
        self.timeLineBackground.move(0, 0)
        self.timeLineBackground.show()
        self.addWidget(self.timeLineBackground)

        self.lineHover = QSvgWidget('graphics/timeLineLineHover.svg')
        self.addWidget(self.lineHover)
        self.lineHover.setFixedHeight(self.height)
        self.lineHover.setFixedWidth(2)
        self.lineHover.show()
        self.lineHover.setHidden(True)

        for f in range(self.timeline.frames):
            self.furthestLine = self.timeline.frameGap * f

            frameLabel = QLabel(str(f))
            self.addWidget(frameLabel)
            frameLabel.show()
            frameLabel.move((self.timeline.frameGap * f) + (self.timeline.frameGap / 2) - (frameLabel.width() / 2), 2)
            frameLabel.setStyleSheet("background-color: transparent;")
            self.childLabels.append(frameLabel)

            if f == self.session.selectedFrame:
                self.lineSelected = QSvgWidget('graphics/timeLineLineSelected.svg')
                self.addWidget(self.lineSelected)
                self.lineSelected.setFixedHeight(self.height)
                self.lineSelected.setFixedWidth(2)
                self.lineSelected.show()
                self.lineSelected.move(self.timeline.frameGap * f, 0)
                self.childLines.append(self.lineSelected)
            if not f % (self.timeline.fps / 2):
                lineStrong = QSvgWidget('graphics/timeLineLineStrong.svg')
                self.addWidget(lineStrong)
                lineStrong.setFixedHeight(self.height)
                lineStrong.setFixedWidth(1)
                lineStrong.show()
                lineStrong.move(self.timeline.frameGap * f, 0)
                self.childLines.append(lineStrong)
            else:
                lineWeak = QSvgWidget('graphics/timeLineLineWeak.svg')
                self.addWidget(lineWeak)
                lineWeak.setFixedHeight(self.height)
                lineWeak.setFixedWidth(1)
                lineWeak.show()
                lineWeak.move(self.timeline.frameGap * f, 0)
                self.childLines.append(lineWeak)

        self.timeLineBackground.setFixedSize(self.furthestLine + self.timeline.frameGap,
                                             self.height)
        self.timeline.graphicsView.fitInView(0, 0, self.timeline.frameGap * 20 - 5,
                                             self.timeline.graphicsView.height())

        marker = QSvgWidget('graphics/timeLineMarker.svg')
        self.addWidget(marker)
        marker.setFixedSize(10, 20)
        marker.move((self.session.selectedFrame * self.timeline.frameGap) + (self.timeline.frameGap / 2) - (marker.width() / 2),
                    (self.height / 2) - (marker.height() / 2))
        marker.show()
        marker.setStyleSheet("background-color: transparent;")

        self.session.keyFrames[0] = {'marker': marker}

    def reDrawWidgets(self):
        [item.deleteLater() for item in self.childLines]
        [item.deleteLater() for item in self.childLabels]
        self.childLines = []
        self.childLabels = []

        for f in range(self.timeline.frames):
            self.furthestLine = self.timeline.frameGap * f

            frameLabel = QLabel(str(f))
            self.addWidget(frameLabel)
            frameLabel.show()
            frameLabel.move((self.timeline.frameGap * f) + (self.timeline.frameGap / 2) - (frameLabel.width() / 2), 2)
            self.childLabels.append(frameLabel)

            if f == self.session.selectedFrame:
                self.lineSelected = QSvgWidget('graphics/timeLineLineSelected.svg')
                self.addWidget(self.lineSelected)
                self.lineSelected.setFixedHeight(self.height)
                self.lineSelected.setFixedWidth(2)
                self.lineSelected.show()
                self.lineSelected.move(self.timeline.frameGap * f, 0)
                self.childLines.append(self.lineSelected)
            if not f % (self.timeline.fps / 2):
                lineStrong = QSvgWidget('graphics/timeLineLineStrong.svg')
                self.addWidget(lineStrong)
                lineStrong.setFixedHeight(self.height)
                lineStrong.setFixedWidth(1)
                lineStrong.show()
                lineStrong.move(self.timeline.frameGap * f, 0)
                self.childLines.append(lineStrong)
            else:
                lineWeak = QSvgWidget('graphics/timeLineLineWeak.svg')
                self.addWidget(lineWeak)
                lineWeak.setFixedHeight(self.height)
                lineWeak.setFixedWidth(1)
                lineWeak.show()
                lineWeak.move(self.timeline.frameGap * f, 0)
                self.childLines.append(lineWeak)

        self.timeLineBackground.setFixedSize(self.furthestLine + self.timeline.frameGap,
                                        self.height)

    def mouseMoveEvent(self, event):
        self.lineHover.setHidden(False)
        frame = floor((event.scenePos().x() / self.timeline.frameGap) + .3)
        self.lineHover.move(self.timeline.frameGap * frame, 0)

        visibleArea = self.timeline.graphicsView.mapToScene(self.timeline.graphicsView.rect()).boundingRect()
        if event.scenePos().x() < visibleArea.x() + 3 or(
                event.scenePos().x() > (visibleArea.x() + visibleArea.width()) - 4):
            self.lineHover.setHidden(True)
        if event.scenePos().y() < 0 or event.scenePos().y() > 120:
            self.lineHover.setHidden(True)

        super(TimeLineScene, self).mouseMoveEvent(event)

    def mouseReleaseEvent(self, event):
        frame = floor((event.scenePos().x() / self.timeline.frameGap) + .3)
        if frame > self.timeline.frames - 1:
            frame = self.timeline.frames - 1

        self.session.selectFrame(frame)

        super(TimeLineScene, self).mouseReleaseEvent(event)

    def wheelEvent(self, event):
        scrollBar = self.timeline.graphicsView.horizontalScrollBar()
        scrollBar.setValue(scrollBar.value() - (event.delta() / 2))

        super(TimeLineScene, self).wheelEvent(event)


class TimeLine(QHBoxLayout):
    def __init__(self, size, session, *args, **kwargs):
        super(TimeLine, self).__init__(*args, **kwargs)

        self.fps = 12
        self.frames = 100
        self.frameGap = 25
        self.size = size
        self.height = size.height() - 5
        self.setContentsMargins(0, 0, 0, 0)
        self.session = session

        self.graphicsScene = TimeLineScene(self, self.height, session)
        self.graphicsView = QGraphicsView(self.graphicsScene)
        self.addWidget(self.graphicsView)
        self.graphicsScene.drawWidgets()

        self.graphicsView.setFixedHeight(size.height())
        self.graphicsView.setFocusPolicy(Qt.NoFocus)
        self.graphicsView.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.graphicsView.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.graphicsView.setStyleSheet("background: transparent")
        self.graphicsView.show()

    def addMarker(self, frame=False):
        marker = QSvgWidget('graphics/timeLineMarker.svg')
        self.graphicsScene.addWidget(marker)
        marker.setFixedSize(10, 20)
        if not frame:
            frame = self.session.selectedFrame
        marker.move((frame * self.frameGap) + (self.frameGap / 2) - (marker.width() / 2),
                    (self.height / 2) - (marker.height() / 2))
        marker.setStyleSheet("background-color: transparent;")
        marker.show()

        self.session.keyFrames[self.session.selectedFrame] = {'marker': marker}

        return marker

    def selectFrame(self, frame):
        self.graphicsScene.lineSelected.move(self.frameGap * frame, 0)


class FramesSpinBox(QSpinBox):
    def __init__(self, *args, **kwargs):
        super(FramesSpinBox, self).__init__(*args, **kwargs)

        self.setButtonSymbols(QAbstractSpinBox.NoButtons)

    def wheelEvent(self, event):
        pass
