import sys
import json

from PyQt5.QtWidgets import (QApplication, QMainWindow, QMessageBox,
                             QWidget, QLabel, QPushButton, QFileDialog,
                             QFrame, QListWidget, QListWidgetItem, QComboBox,
                             QScrollArea, QVBoxLayout, QTextBrowser,
                             QSpinBox, QAction)
from PyQt5.QtCore import Qt, QTimer

from ui import Ui_Form
from effectUi import Ui_Effect
from widgets import LayerNameTextBox, genericEffectEditor
from timeline import TimeLine
from backend import Session


class MainWindow(QMainWindow):
    def __init__(self, desktop):
        super(MainWindow, self).__init__()
        self.screenSize = desktop.screenGeometry()
        self.mainWidget = self.getWidget()
        self.setCentralWidget(self.mainWidget)
        self.setWindowTitle('QTTestApp')
        # QApplication.setStyle('breeze')
        QApplication.setStyle('adwaita-dark')

        with open('effects.json', 'r') as f:
            self.effectJson = json.loads(f.read())

        self.session = Session(self)

        self.getChildren()
        self.addEffectContainerOriginalPosition = self.addEffectContainer.pos()
        self.setupMenuBar()
        self.session.initLayers()
        self.setupUi()
        self.initSignals()

    def delay(self, function, time):
        QTimer().singleShot(time, function)

    def setupMenuBar(self):
        bar = self.menuBar()
        file = bar.addMenu("File")

        save = QAction("Save", self)
        save.setShortcut("Ctrl+S")
        save.triggered.connect(self.save)

        saveAs = QAction("SaveAs", self)
        saveAs.setShortcut("Ctrl+Shift+S")
        saveAs.triggered.connect(self.saveAs)

        load = QAction("Load", self)
        load.setShortcut("Ctrl+O")
        load.triggered.connect(self.load)

        file.addAction(save)
        file.addAction(saveAs)
        file.addAction(load)

    def load(self):
        name = QFileDialog.getOpenFileName(self, filter='*.py')[0]
        if name:
            self.session.load(name)

    def save(self):
        if not self.session.lastSaveFileName:
            name = QFileDialog.getSaveFileName(self, filter='*.py')[0]
            if name:
                self.session.save(name)
        else:
            self.session.save()

    def saveAs(self):
        name = QFileDialog.getSaveFileName(self, filter='*.py')[0]
        if name:
            self.session.save(name)

    def setupUi(self):
        self.width = 1320
        self.height = 680
        self.setGeometry((self.screenSize.width() / 2) - self.width / 2,
                         (self.screenSize.height() / 2) - self.height / 2,
                         self.width,
                         self.height)
        self.mainWidget.show()
        self.inputModeLabel.setHidden(True)
        self.cloneLayerComboBox.setHidden(True)

        self.timeLine = TimeLine(self.timeLineContainer.size(), self.session)
        self.timeLineContainer.setLayout(self.timeLine)
        self.FPSSpinBox.setValue(self.timeLine.fps)

        self.outputLabel.setText(self.session.outputPath)

        self.show()

    def drawEffects(self):
        for layerId, layer in self.session.layers.items():
            i = 0
            for effectId, effect in self.session.getEffects(layerId).items():
                effectWidget = QWidget(self.effectArea)
                ui = Ui_Effect()
                ui.setupUi(effectWidget)
                effectWidget.move(effectWidget.x()
                                + (effectWidget.width()
                                    * i),
                                effectWidget.y())

                self.addEffectContainer.move(self.addEffectContainer.x() + effectWidget.width(),
                                            self.addEffectContainer.y())
                effectWidget.show()

                self.effectArea.setMinimumWidth((effectWidget.width()
                                                * i)
                                                + (self.addEffectContainer.width() * 1.3))
                self.effectScrollArea.horizontalScrollBar().setValue(effectWidget.width() * len(self.session.getEffects(self.session.selectedLayer).keys()))

                comboBox = effectWidget.findChild(QComboBox, 'effectComboBox')
                comboBox.setCurrentIndex(comboBox.findText(effect['effect']))
                effectSettingsContainer = effectWidget.findChild(QWidget, 'effectSettingsContainer')
                attr = self.effectJson[comboBox.currentText()]
                editor = genericEffectEditor(effectId, comboBox.currentText(), attr,
                                            self.session, effectSettingsContainer)
                comboBox.currentTextChanged.connect(self.onEffectComboBoxChange)
                comboBox.showPopup()

                effect['widget'] = effectWidget
                effect['editor'] = editor

                if self.session.selectedFrame != 0:
                    editor.setupMarkers({})

                for name, value in editor.attr.items():
                    self.session.addKeyFrame(0, effectId, name, value)

                i += 1

    def drawLayers(self):
        for layerId, layer in self.session.layers.items():
            if layerId != 'Root':
                self.addLayer(layerId)

    def drawKeyframes(self):
        for frame, effects in self.session.keyFrames.items():
            effects['marker'] = self.timeLine.addMarker(frame)

    def clear(self):
        for effectId, effect in self.session.effects.items():
            effect['widget'].deleteLater()
            effect['editor'].deleteLater()
        self.addEffectContainer.move(self.addEffectContainerOriginalPosition)

        for layerId, layer in self.session.layers.items():
            self.layersList.takeItem(self.layersList.row(layer['item']))

        self.timeLine.selectFrame(0)
        for frame, effects in self.session.keyFrames.items():
            effects['marker'].deleteLater()

    def onAddLayerBtnClicked(self):
        self.newLayer = QListWidgetItem()
        newLayerIndex = self.layersList.currentRow()

        layersToMove = []
        if newLayerIndex > 0:
            for row in range(newLayerIndex):
                layersToMove.append(self.layersList.takeItem(0))

        self.layersList.insertItem(0, self.newLayer)

        tmpNameBox = LayerNameTextBox('Name...',
                                      self.session.layers,
                                      returnFunction=self.completeAddLayerAction,
                                      cancelFunction=self.cancelAddLayer)
        self.layersList.setItemWidget(self.newLayer, tmpNameBox)
        tmpNameBox.setFocus(True)
        self.layersList.setCurrentRow(0)
        self.layerBlendModeBox.setEnabled(False)
        self.delLayerBtn.setEnabled(False)
        self.addLayerBtn.setEnabled(False)

        for item in layersToMove[::-1]:
            self.layersList.insertItem(0, item)

    def cancelAddLayer(self, *args):
        self.layersList.removeItemWidget(self.newLayer)
        self.layersList.takeItem(self.layersList.row(self.newLayer))
        self.layerBlendModeBox.setEnabled(True)
        self.delLayerBtn.setEnabled(True)
        self.addLayerBtn.setEnabled(True)

    def completeAddLayerAction(self, name, *args):
        self.layersList.takeItem(self.layersList.row(self.newLayer))
        self.addLayer(name)
        self.selectLayer(name)

        self.layerBlendModeBox.setEnabled(True)
        self.delLayerBtn.setEnabled(True)
        self.addLayerBtn.setEnabled(True)

    def addLayer(self, name):
        item = QListWidgetItem(name)
        layersToMove = []
        newLayerIndex = self.layersList.currentRow()

        if newLayerIndex > 0:
            for row in range(newLayerIndex):
                layersToMove.append(self.layersList.takeItem(0))

        self.layersList.insertItem(0, item)
        item.setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable)

        for item in layersToMove[::-1]:
            self.layersList.insertItem(0, item)

        self.session.addLayer(name, item)

    def onDelLayerBtnClicked(self):
        choice = QMessageBox.question(self, 'Delete Layer',
                                      'Do you really want to delete this layer?\n\n{}'.format(self.session.selectedLayer))
        if choice == QMessageBox.Yes:
            layer = self.session.selectedLayer
            item = self.session.layers[layer]['item']

            self.session.delLayer(layer)
            self.layersList.takeItem(self.layersList.row(item))

    def getChildren(self):
        self.layersList = self.findChild(QListWidget, 'layersList')
        self.layerBlendModeBox = self.findChild(QComboBox, 'layerBlendModeBox')
        self.delLayerBtn = self.findChild(QPushButton, 'delLayerBtn')
        self.addLayerBtn = self.findChild(QPushButton, 'addLayerBtn')
        self.layerOpacityBox = self.findChild(QSpinBox, 'layerOpacityBox')

        self.effectArea = self.findChild(QWidget, 'effectArea')
        self.effectScrollArea = self.findChild(QScrollArea, 'effectScrollArea')

        self.inputModeLabel = self.findChild(QLabel, 'inputModeLabel')
        self.inputImageBtn = self.findChild(QPushButton, 'inputImageBtn')
        self.inputVideoBtn = self.findChild(QPushButton, 'inputVideoBtn')
        self.inputCloneLayerBtn = self.findChild(QPushButton, 'inputCloneLayerBtn')
        self.cloneLayerComboBox = self.findChild(QComboBox, 'cloneLayerComboBox')

        self.FPSSpinBox = self.findChild(QSpinBox, 'FPSSpinBox')
        self.status = self.findChild(QLabel, 'statusBarLabel')

        self.addEffectContainer = self.findChild(QFrame, 'addEffectContainer')
        self.addEffectBtn = self.findChild(QPushButton, 'addEffectBtn')

        self.renderBtn = self.findChild(QPushButton, 'renderBtn')
        self.outputLabel = self.findChild(QTextBrowser, 'outputLabel')

        self.timeLineContainer = self.findChild(QWidget, 'timeLineContainer')

    def initSignals(self):
        # Signals
        self.layersList.currentItemChanged.connect(self.onLayerChange)
        self.layersList.itemDoubleClicked.connect(self.onLayerDoubleClick)
        self.layerBlendModeBox.currentTextChanged.connect(self.onBlendModeChange)
        self.addLayerBtn.clicked.connect(self.onAddLayerBtnClicked)
        self.delLayerBtn.clicked.connect(self.onDelLayerBtnClicked)
        self.layerOpacityBox.valueChanged.connect(self.onLayerOpacityChanged)

        self.inputImageBtn.clicked.connect(self.onInputImageBtnClicked)
        self.inputVideoBtn.clicked.connect(self.onInputVideoBtnClicked)
        self.inputCloneLayerBtn.clicked.connect(self.onInputCloneLayerBtnClicked)
        self.cloneLayerComboBox.currentTextChanged.connect(self.onCloneLayerComboBoxChanged)

        self.FPSSpinBox.valueChanged.connect(self.onFPSChanged)
        self.renderBtn.clicked.connect(self.onRenderBtnClicked)

        self.addEffectBtn.clicked.connect(self.onAddEffectBtnClicked)

        # Flags
        self.cloneLayerComboBoxIgnoreChanges = False

    def onRenderBtnClicked(self):
        self.session.write('gmicAnimator/tmp.py')

    def onLayerOpacityChanged(self, value):
        self.session.layers[self.session.selectedLayer]['opacity'] = value

    def onCloneLayerComboBoxChanged(self, layerToClone):
        if len(layerToClone) > 0 and not self.cloneLayerComboBoxIgnoreChanges:
            self.session.layers[self.session.selectedLayer]['inputPath'] = layerToClone

    def onAddEffectBtnClicked(self):
        effect = QWidget(self.effectArea)
        ui = Ui_Effect()
        ui.setupUi(effect)
        effect.move(effect.x() + (effect.width() * len(self.session.getEffects(self.session.selectedLayer).keys())), effect.y())
        effectId = self.session.addEffect()

        self.addEffectContainer.move(self.addEffectContainer.x() + effect.width(),
                                     self.addEffectContainer.y())
        effect.show()

        self.effectArea.setMinimumWidth((effect.width() * len(self.session.getEffects(self.session.selectedLayer).keys())) + (self.addEffectContainer.width() * 1.3))
        self.effectScrollArea.horizontalScrollBar().setValue(effect.width() * len(self.session.getEffects(self.session.selectedLayer).keys()))

        comboBox = effect.findChild(QComboBox, 'effectComboBox')
        effectSettingsContainer = effect.findChild(QWidget, 'effectSettingsContainer')
        attr = self.effectJson[comboBox.currentText()]
        editor = genericEffectEditor(effectId, comboBox.currentText(), attr,
                                     self.session, effectSettingsContainer)
        comboBox.currentTextChanged.connect(self.onEffectComboBoxChange)
        comboBox.showPopup()

        self.session.effects[effectId].update({'widget': effect,
                                               'editor': editor,
                                               'effect': comboBox.currentText()})

        if self.session.selectedFrame != 0:
            editor.setupMarkers({})

        for name, value in editor.attr.items():
            self.session.addKeyFrame(0, effectId, name, value)

    def onFPSChanged(self, value):
        self.timeLine.fps = value
        self.session.fps = value

    def onEffectComboBoxChange(self, effect):
        #TODO add some sort of warning if the effect has data
        comboBox = self.sender()
        editor = comboBox.parentWidget().parentWidget().findChild(QWidget, 'effectSettingsContainer').findChild(QVBoxLayout, 'effectEditor')
        attr = self.effectJson[effect]
        editor.changeEffect(effect, attr)
        if self.session.selectedFrame != 0:
            editor.setupMarkers({})
        self.session.renameEffect(editor.effectId, effect, attr)

    def hideEffects(self):
        [item[1]['widget'].setHidden(True) for item in self.session.effects.items()]
        self.addEffectContainer.move(self.addEffectContainerOriginalPosition)

    def showEffects(self):
        for __, item in self.session.getEffects(self.session.selectedLayer).items():
            item['widget'].setHidden(False)
            self.addEffectContainer.move(self.addEffectContainer.x() + item['widget'].width(),
                                         self.addEffectContainer.y())

    def onInputVideoBtnClicked(self):
        name = QFileDialog.getOpenFileName(self, filter='*.mp4')
        if name[0]:
            self.session.layers[self.session.selectedLayer]['inputMode'] = 'Video'
            self.session.layers[self.session.selectedLayer]['inputPath'] = name[0]
            self.showInputModeLabel('Mode:\tVideo\nPath:\t{}'.format(name[0]))

    def showInputModeLabel(self, text):
        self.inputModeLabel.setHidden(False)
        self.inputModeLabel.setText(text)
        self.addEffectBtn.setEnabled(True)
        self.inputImageBtn.setHidden(True)
        self.inputVideoBtn.setHidden(True)
        self.inputCloneLayerBtn.setHidden(True)
        self.addLayerBtn.setEnabled(True)

    def onInputCloneLayerBtnClicked(self):
        layer = 'Root'
        self.session.layers[self.session.selectedLayer]['inputMode'] = 'Clone'
        self.session.layers[self.session.selectedLayer]['inputPath'] = layer

        self.showInputModeLabel('Mode:\tClone Layer')
        self.cloneLayerComboBox.clear()
        for key in self.session.layers.keys():
            if key != self.session.selectedLayer:
                self.cloneLayerComboBox.addItem(key)
                self.cloneLayerComboBox.setHidden(False)
                self.cloneLayerComboBox.showPopup()
                self.cloneLayerComboBox.setCurrentIndex(self.cloneLayerComboBox.findText(layer))

    def onInputImageBtnClicked(self):
        name = QFileDialog.getOpenFileName(self, filter='*.png')
        if name[0]:
            self.session.layers[self.session.selectedLayer]['inputMode'] = 'Image'
            self.session.layers[self.session.selectedLayer]['inputPath'] = name[0]

            self.showInputModeLabel('Mode:\tImage\nPath:\t{}'.format(name[0]))

    def onLayerDoubleClick(self, layer):
        if layer.text() != 'Root':
            tmpNameBox = LayerNameTextBox(layer.text(),
                                          self.session.layers,
                                          returnFunction=self.renameLayer,
                                          cancelFunction=self.cancelRenameLayer)
            self.layersList.setItemWidget(self.session.layers[layer.text()]['item'], tmpNameBox)

    def cancelRenameLayer(self, previousName):
        self.layersList.removeItemWidget(self.session.layers[previousName]['item'])

    def renameLayer(self, name, previousName):
        if name not in self.session.layers.keys():
            for item in self.session.layers.items():
                if item[1]['inputPath'] == previousName:
                    self.session.layers[item[0]]['inputPath'] = name

            for effectId, effect in self.session.effects.items():
                if effect['layer'] == previousName:
                    effect['layer'] = name

            self.session.layers[previousName]['item'].setText(name)
            self.layersList.removeItemWidget(self.session.layers[previousName]['item'])
            self.session.layers[name] = self.session.layers.pop(previousName)
            print(self.session.layers[name])
            print(self.session.effects)
            self.selectLayer(name)

    def onBlendModeChange(self, mode):
        self.session.layers[self.session.selectedLayer]['blendMode'] = mode

    def onLayerChange(self, newLayer):
        if newLayer:
            self.selectLayer(newLayer.text())

    def selectLayer(self, name):
        if self.session.selectLayer(name):
            self.layersList.setCurrentRow(self.layersList.row(self.session.layers[self.session.selectedLayer]['item']))
            self.layerBlendModeBox.setCurrentIndex(self.layerBlendModeBox.findText(self.session.layers[self.session.selectedLayer]['blendMode']))
            self.layerOpacityBox.setValue(self.session.layers[self.session.selectedLayer]['opacity'])
            self.hideEffects()
            self.showEffects()
            self.effectArea.setMinimumWidth(0)

            if self.session.getEffects(self.session.selectedLayer):
                effectId = list(self.session.getEffects(self.session.selectedLayer).keys())[0]
                self.effectArea.setMinimumWidth((self.session.getEffects(self.session.selectedLayer)[effectId]['widget'].width()
                                                 * len(self.session.getEffects(self.session.selectedLayer).keys()))
                                                + (self.addEffectContainer.width() * 1.3))

            if self.session.selectedLayer != 'Root':
                self.layerBlendModeBox.setEnabled(True)
                self.delLayerBtn.setEnabled(True)
                self.inputCloneLayerBtn.setEnabled(True)
                self.layerOpacityBox.setEnabled(True)
                rootIsSelected = False
            else:
                self.layerBlendModeBox.setEnabled(False)
                self.delLayerBtn.setEnabled(False)
                self.inputCloneLayerBtn.setEnabled(False)
                self.layerOpacityBox.setEnabled(False)
                rootIsSelected = True

            if self.session.layers[self.session.selectedLayer]['inputMode'] == None:
                # self.addEffectBtn.setEnabled(False)
                self.inputImageBtn.setHidden(False)
                self.inputVideoBtn.setHidden(False)
                self.inputCloneLayerBtn.setHidden(False)

                self.inputModeLabel.setHidden(True)
                self.cloneLayerComboBox.setHidden(True)

                if rootIsSelected:
                    self.addLayerBtn.setEnabled(False)
            else:
                if self.session.layers[self.session.selectedLayer]['inputMode'] == 'Clone':
                    self.cloneLayerComboBox.setHidden(False)
                    self.showInputModeLabel('Mode:\tClone Layer')
                    self.cloneLayerComboBoxIgnoreChanges = True
                    self.cloneLayerComboBox.clear()
                    for key in self.session.layers.keys():
                        if key != self.session.selectedLayer:
                            self.cloneLayerComboBox.addItem(key)
                            self.cloneLayerComboBoxIgnoreChanges = False
                            self.cloneLayerComboBox.setCurrentIndex(self.cloneLayerComboBox.findText(self.session.layers[self.session.selectedLayer]['inputPath']))
                else:
                    self.cloneLayerComboBox.setHidden(True)
                    self.inputModeLabel.setText('Mode:\t{}\nPath:\t{}'.format(self.session.layers[self.session.selectedLayer]['inputMode'], self.session.layers[self.session.selectedLayer]['inputPath']))

                if rootIsSelected:
                    self.addLayerBtn.setEnabled(True)

                self.inputModeLabel.setHidden(False)
                self.addEffectBtn.setEnabled(True)
                self.inputImageBtn.setHidden(True)
                self.inputVideoBtn.setHidden(True)
                self.inputCloneLayerBtn.setHidden(True)

            self.effectScrollArea.horizontalScrollBar().setValue(self.session.layers[self.session.selectedLayer]['lastScrollPosition'])

    def exitApp(self):
        choice = QMessageBox.question(self, 'Quit?',
                                      'Do you really want to leave the application?')
        if choice == QMessageBox.Yes:
            print('Shutting down...')
            sys.exit()

    def getWidget(self):
        Form = QWidget()
        ui = Ui_Form()
        ui.setupUi(Form)
        return Form


if __name__ == '__main__':
    app = QApplication(sys.argv)
    a = MainWindow(app.desktop())
    sys.exit(app.exec_())
