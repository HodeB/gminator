from sh import rm
import json


class Load():
    def __init__(self, path):
        self.path          = path
        self.frame         = 0
        self.layer         = 'Root'
        self.selectedLayer = 'Root'

        self.effects       = {}
        self.keyframes     = {}
        self.layers        = {'Root': {}}

        with open('effects.json', 'r') as f:
            self.effectJson = json.loads(f.read())

        self.generateTempCode()
        from tmpGenerator import generateState
        generateState(self)
        rm('tmpGenerator.py')

    def generateTempCode(self):
        with open(self.path, 'r+') as source:
            with open('tmpGenerator.py', 'w+') as generated:
                generated.write('def generateState(loader):\n')
                for line in source.readlines()[5:-1]:
                    if line == '\n':
                        generated.write(line)
                    elif line[0] != ' ':
                        generated.write('    loader.{}'.format(line))
                    else:
                        generated.write('    {}'.format(line))

    def add(self, id, effect, attr=False):
        self.effects.update({id: {'effect': effect,
                                  'layer': self.selectedLayer}})
        if not attr:
            attr = self.effectsJson[effect]

        if 0 not in self.keyframes.keys():
            self.keyframes[0] = {}

        for name, value in attr.items():
            if id not in self.keyframes[0].keys():
                self.keyframes[0][id] = {name: value}
            else:
                self.keyframes[0][id][name] = value

    def animate(self, id, endAt, attr):
        f = endAt + self.frame
        for name, value in attr.items():
            if f not in self.keyframes.keys():
                self.keyframes[f] = {id: {name: value}}
            elif id not in self.keyframes[f].keys():
                self.keyframes[f][id] = {name: value}
            else:
                self.keyframes[f][id][name] = value

    def setOutputDir(self, path):
        self.outputPath = path

    def keyframe(self, stop):
        self.frame = int(stop)

    def openVideo(self, path):
        self.layers[self.selectedLayer]['inputMode'] = 'Video'
        self.layers[self.selectedLayer]['inputPath'] = path

    def openImage(self, path):
        self.layers[self.selectedLayer]['inputMode'] = 'Image'
        self.layers[self.selectedLayer]['inputPath'] = path

    def openImageStack(self, path):
        self.layers[self.selectedLayer]['inputMode'] = 'Stack'
        self.layers[self.selectedLayer]['inputPath'] = path

    def openLayer(self, layer):
        self.layers[self.selectedLayer]['inputMode'] = 'Clone'
        self.layers[self.selectedLayer]['inputPath'] = layer

    def setSize(self, x, y):
        self.size = (int(x), int(y))

    def getSize(self):
        return self.size

    def delete(self):
        pass

    def setFps(self, fps):
        self.fps = int(fps)

    def getFps(self):
        return self.fps

    def animateReturn(self, endAt):
        pass

    def addLayer(self, id, mix, opacity=1):
        self.layers.update({id: {'blendMode': mix.title(),
                                 'opacity': float(opacity) * 100}})

    def delLayer(self, id):
        pass

    def useLayer(self, id):
        self.selectedLayer = id
