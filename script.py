from userfacing import (add, play, keyframe, animate, setOutputDir,
                        openImage, setSize, setFps, addLayer,
                        useLayer, animateReturn, getSize, getFps,
                        delLayer, openVideo, openImageStack, openLayer)

setSize(640, 360)
setOutputDir('outputDir/')
setFps(15)

openImage('/home/martin/black.png')
add('0', 'cartoon', {
    'smoothness': 3,
    'sharpening': 150,
    'threshold': 20,
    'thickness': 25,
    'color': 15,
    'quantization': 8})
add('1', 'cartoon', {
    'smoothness': 3,
    'sharpening': 150,
    'threshold': 20,
    'thickness': 25,
    'color': 15,
    'quantization': 8})

addLayer('Fda', 'soft light', 0.78)
useLayer('Fda')
openLayer('Root')
add('2', 'rotoidoscope', {
    'centerx': 50,
    'centery': 50,
    'tiles': 10,
    'smoothness': 0.2,
    'boundaryconditions': 3})
add('3', 'cartoon', {
    'smoothness': 3,
    'sharpening': 150,
    'threshold': 20,
    'thickness': 25,
    'color': 15,
    'quantization': 8})
add('4', 'cartoon', {
    'smoothness': 3,
    'sharpening': 150,
    'threshold': 20,
    'thickness': 25,
    'color': 15,
    'quantization': 8})
add('5', 'cartoon', {
    'smoothness': 3,
    'sharpening': 150,
    'threshold': 20,
    'thickness': 25,
    'color': 15,
    'quantization': 8})

useLayer('Root')
animate('0', 14, {'sharpening': 139})
animate('0', 14, {'threshold': 9})
animate('1', 4, {'sharpening': 155})
animate('1', 14, {'thickness': 14})
animate('1', 14, {'threshold': 14})
useLayer('Fda')
animate('2', 4, {'tiles': 0})
animate('2', 9, {'centery': 59})

keyframe(14)
play()