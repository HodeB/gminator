# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qml/effect.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Effect(object):
    def setupUi(self, Effect):
        Effect.setObjectName("Effect")
        Effect.resize(250, 492)
        self.frame = QtWidgets.QFrame(Effect)
        self.frame.setGeometry(QtCore.QRect(0, 0, 251, 491))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout.setObjectName("verticalLayout")
        self.widget = QtWidgets.QWidget(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy)
        self.widget.setMinimumSize(QtCore.QSize(0, 40))
        self.widget.setObjectName("widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.effectComboBox = QtWidgets.QComboBox(self.widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.effectComboBox.sizePolicy().hasHeightForWidth())
        self.effectComboBox.setSizePolicy(sizePolicy)
        self.effectComboBox.setMinimumSize(QtCore.QSize(0, 30))
        self.effectComboBox.setMaximumSize(QtCore.QSize(16777215, 30))
        self.effectComboBox.setFocusPolicy(QtCore.Qt.NoFocus)
        self.effectComboBox.setInsertPolicy(QtWidgets.QComboBox.InsertAtTop)
        self.effectComboBox.setObjectName("effectComboBox")
        self.effectComboBox.addItem("")
        self.effectComboBox.addItem("")
        self.horizontalLayout.addWidget(self.effectComboBox)
        self.verticalLayout.addWidget(self.widget)
        self.effectSettingsContainer = QtWidgets.QWidget(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.effectSettingsContainer.sizePolicy().hasHeightForWidth())
        self.effectSettingsContainer.setSizePolicy(sizePolicy)
        self.effectSettingsContainer.setMinimumSize(QtCore.QSize(0, 343))
        self.effectSettingsContainer.setObjectName("effectSettingsContainer")
        self.verticalLayout.addWidget(self.effectSettingsContainer)
        self.verticalLayout.setStretch(1, 16)

        self.retranslateUi(Effect)
        QtCore.QMetaObject.connectSlotsByName(Effect)

    def retranslateUi(self, Effect):
        _translate = QtCore.QCoreApplication.translate
        Effect.setWindowTitle(_translate("Effect", "Form"))
        self.effectComboBox.setItemText(0, _translate("Effect", "cartoon"))
        self.effectComboBox.setItemText(1, _translate("Effect", "rotoidoscope"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Effect = QtWidgets.QWidget()
    ui = Ui_Effect()
    ui.setupUi(Effect)
    Effect.show()
    sys.exit(app.exec_())

