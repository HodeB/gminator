from PyQt5.QtWidgets import (QVBoxLayout, QHBoxLayout, QLineEdit, QLabel,
                             QSpacerItem, QSizePolicy, QSpinBox, QWidget,
                             QGraphicsView, QGraphicsScene, QPushButton)
from PyQt5.QtSvg import QSvgWidget
from PyQt5.QtCore import Qt


class LayerNameTextBox(QLineEdit):
    def __init__(self, defaultName, layers, returnFunction, cancelFunction, *args, **kwargs):
        super(LayerNameTextBox, self).__init__()

        self.defaultName = defaultName
        self.insert(self.defaultName)
        self.selectAll()
        self.returnFunction = returnFunction
        self.cancelFunction = cancelFunction
        self.layers = layers

    def focusOutEvent(self, event):
        if self.confirm():
            super(LayerNameTextBox, self).focusOutEvent(event)
        else:
            self.cancelFunction(self.defaultName)

    def keyPressEvent(self, event):
        if event.key() == 16777216:
            self.cancelFunction(self.defaultName)
        if event.key() == 16777220:
            self.confirm()

        super(LayerNameTextBox, self).keyPressEvent(event)

        if len(self.text()):
            if self.text()[0] == ' ':
                self.clear()
            else:
                self.setText(self.text()[0].upper() + self.text()[1:])

    def confirm(self):
        if self.text() != '' and self.text() != self.defaultName and self.text() not in self.layers.keys():
            self.returnFunction(self.text(), self.defaultName)
            return True
        else:
            return False


class genericEffectEditor(QVBoxLayout):
    def __init__(self, effectId, effect, attributes, session, *args, **kwargs):
        super(genericEffectEditor, self).__init__(*args, **kwargs)

        self.setObjectName('effectEditor')
        self.effect = effect
        self.defaultAttr = {}
        self.defaultAttr.update(attributes)
        self.attr = self.defaultAttr
        self.childWidgets = []
        self.childEditors = []
        self.childMarkers = {}
        self.effectId = effectId
        self.session = session

        self.setupEditor(self.attr)

    def setupEditor(self, attributes):
        for key, value in attributes.items():
            layout = QHBoxLayout()
            self.addLayout(layout)

            label = QLabel(key)
            layout.addWidget(label)

            editor = QSpinBox()
            editor.setMaximum(99999999)  # TODO dont do this
            editor.setValue(value)
            editor.setMaximumWidth(100)
            editor.setFocusPolicy(Qt.ClickFocus)
            editor.setObjectName(key)
            editor.valueChanged.connect(self.onAttrValueChange)

            marker = QSvgWidget('graphics/timeLineMarker.svg')
            marker.setFixedSize(8, 16)
            self.childMarkers[key] = marker

            layout.addWidget(marker)
            layout.addWidget(editor)

            self.childWidgets.append(layout)
            self.childWidgets.append(label)
            self.childEditors.append(editor)

        self.spacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.addSpacerItem(self.spacer)

    def setupMarkers(self, keyframe):
        ignore = []
        for name, value in keyframe.items():
            self.childMarkers[name].setHidden(False)
            ignore.append(name)

        for name, marker in self.childMarkers.items():
            if name not in ignore:
                marker.setHidden(True)

    def onAttrValueChange(self, value):
        name = self.sender().objectName()

        self.attr[name] = value
        self.session.updateEffect(self.effectId, name, value)

        self.childMarkers[name].setHidden(False)

    def disableEditor(self):
        [item.setEnabled(False) for item in self.childEditors]

    def enableEditor(self):
        [item.setEnabled(True) for item in self.childEditors]

    def clearEditor(self):
        self.removeItem(self.spacer)
        [item.deleteLater() for item in self.childWidgets]
        [item.deleteLater() for item in self.childEditors]
        [item.deleteLater() for __, item in self.childMarkers.items()]
        self.childWidgets = []
        self.childEditors = []
        self.childMarkers = {}

    def changeEffect(self, effect, attributes):
        self.clearEditor()
        self.effect = effect
        self.defaultAttr = attributes
        self.attr = attributes
        self.setupEditor(attributes)
